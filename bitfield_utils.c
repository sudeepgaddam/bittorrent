#include "header.h"
/*
 * 
 * Set all bits in bitfield as 1
 * Assumes memory for bitfield is allocated
 */
 
void set_bitfield(int piece_bytes) 
{	int i;
	
	
	//Todo: This is just a workaround to aviod testing corner case. 
	for (i=0; i<piece_bytes-1; i++) {
		*(piece_bits.bitfield_p + i)  = 0xff;
		*(requested_bits.bitfield_p + i)  = 0xff;
	}
	uint8_t mask;
	int k;
	
	for (k=0; k< piece_count%8; k++) {
		mask |= (0x1<<(7-k));
	}
	*(piece_bits.bitfield_p + i) = mask;
	*(requested_bits.bitfield_p + i) = mask;
}


/*
 * 
 * Set all bits in bitfield as 0
 * Assumes memory for bitfield is allocated
 */
 
void unset_bitfield(int piece_bytes) 
{	int i;
	for (i=0; i<piece_bytes; i++) {
		*(piece_bits.bitfield_p + i)  = 0x00;
	}
}


/*
 * 
 * Bootstrap bitfield initially according to config file
 * Assumes memory for bitfield is allocated
 */
 
void make_bitfield(int piece_bytes) 
{	
	if (thisPeerConfig.thisPeerHasFile) {
		set_bitfield(piece_bytes);
	} else {
		unset_bitfield(piece_bytes);
	}
}

/*
 * 
 * Update exisiting bitfield with the new available piece number
 */
void update_bitfield(int piece_number,uint8_t *bitfieldp) 
{	
	int byte_piece = piece_number/8;
	int bit_index = piece_number%8;
	
	int shift_pos = 8 - bit_index -1;
	*(bitfieldp + byte_piece) |= 0x1 << shift_pos;
	*(requested_bits.bitfield_p + byte_piece) |= 0x1 << shift_pos;

}

/*
 * 
 * Update exisiting bitfield with the requested piece number
 */
void update_request_bits(int piece_number, bitfield_t requested_bits) 
{	
	int byte_piece = piece_number/8;
	int bit_index = piece_number%8;
	
	int shift_pos = 8 - bit_index -1;
	*(requested_bits.bitfield_p + byte_piece) |= 0x1 << shift_pos;

}

/*
 * 
 * Allocate memory for all the neighbouring bits
 */
void alloc_neighbits(int total_neighbours,
					 int piece_bytes,
					 bitfield_t *neigh_bits) 
{	int i;
	for (i=0;i<total_neighbours;i++) {
		(*(neigh_bits + i)).bitfield_p = calloc(piece_bytes, sizeof(uint8_t));
	}
}
/*
 * 
 * Update exisiting neigh bitfield with the new available piece number
 */
void update_neigh_bitfield(int piece_number,
							int index, 
							bitfield_t neigh_bits) 
{	
	int byte_piece = piece_number/8;
	int bit_index = piece_number%8;
	
	int shift_pos = 8 - bit_index -1;
	*(neigh_bits.bitfield_p + byte_piece) |= 0x1 << shift_pos;	//printf("Updated bitfield\n");
}

/*
 * 
 * Check if the neighbour has interesting piece
 * Xor neigh_bits and my_bits 
 * And  
 */
int check_interesting_piece(bitfield_t neigh_bits,
							 bitfield_t my_bits) {
	
	int i;
	uint8_t temp;

	for (i=0;i<piece_bytes;i++) {
		 temp = *(my_bits.bitfield_p + i) ^ *(neigh_bits.bitfield_p +i);
		 temp = temp & *(neigh_bits.bitfield_p + i);

		 if (temp) {
			 //get pos of most significant set bit in temp
			 return 1;
		}
	}
	return 0;
}

/*
 * This assumes that there is an interesting piece
 * To return random interesting piece_id, we get a random 
 * byte from bit field, If there is no interesting piece in it,
 * Get another random byte and check for interesting piece in it
 *  
 * @return -1 if no interesting piece found
 * 			piece_id of random interesting piece
 */
int random_interesting_piece(bitfield_t neigh_bits,
							 bitfield_t my_bits) {
	
	int i,j,k;
	int piece;
	uint8_t temp;
	uint8_t mask = 128;
	
	randomize(rand_array, piece_bytes);
	//print_randomise(rand_array, piece_bytes);
	//printf("After randomise\n");
	for (i=0;i<piece_bytes;i++) {
		 k = *(rand_array + i);
		 temp = *(my_bits.bitfield_p + k) ^ *(neigh_bits.bitfield_p +k);
		 temp = temp & *(neigh_bits.bitfield_p + k);
		if (temp) {
			//printf("Temp npn-zero, k value:%d\n", k);
			for(j=0; j<8; j++) {
				 if (!(temp & mask)) {
					 mask = mask >> 1;
					 //printf("Shifing right\n");
				 } else {
					// printf("Breaking\n");
					 break;
				 }
			}
			 //Got the position j in k'th byte.
			 				// printf("J value:%d", j);
			 if (j < 8) {
				 piece = k*8 + j ;
				 //printf("random_interesting_piece() returning %d\n", piece);
				 return piece;
			 }
		}			 
	}
	return -1;
}

/*
 * 
 * Not needed for now. Directly reading to the destination
 */
/*
void copy_neigh_bitfield  (bitfield_t *dest_neigh_bits,
						   bitfield_t *source_neigh_bits,
						   int piece_bytes) 
{	
	memcpy(dest_neigh_bits->bitfield_p, source_neigh_bits->bitfield_p, sizeof(uint8_t)*piece_bytes);
}*/
