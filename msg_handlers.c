/****************
 * All the Processing functions here
 ******************/
#include "header.h"

 /*
  * set unchoke[peer_id] false
  * 
  */
void proc_choke(int index) {
		printf("got choked by peer ID %d\n", peerConfig[index].peerId);

	is_unchoked[index] = false;
		
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = choking;
	log_data.otherPeerId = peerConfig[index].peerId;
	printLog(log_data);

	
}

 /*
  * set unchoke[peer_id] true
  * Scan through the bitfields and see if we have to request for any piece
  * If true, Then send request message with the piece_id
  * 
  * piece_id should be randomly generated interesting piece
  * 
  * Def Interesting piece: Piece which is not present with us,
  * But present with neighbour
  * 
  */
void proc_unchoke(int index, int sockd) {
	is_unchoked[index] = true;
	int piece_id = random_interesting_piece(neigh_bits[index], requested_bits);
	printf("%d ", piece_id);
	
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = unchoking;
	log_data.otherPeerId = peerConfig[index].peerId;
	printLog(log_data);
	
	if (piece_id > -1) {
		send_request(piece_id, sockd);
	}
	
}
 /*
  * Add to interested list; Write to log, Send unchoke for now. 
  * @todo: Later handle preferred neighbour and optimistically unchoked part
  * 
  */
void proc_interested(int index, int sockd) {
	
	is_interested[index] = true;
	
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = receiveInterest;
	log_data.otherPeerId = peerConfig[index].peerId;
	printLog(log_data);
	
	//send_unchoke(sockd);
	
	
}
 /*
  * 
  * 
  */
void proc_not_interested(int index) {
	is_interested[index] = false;
	
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = receiveNoInterest;
	log_data.otherPeerId = peerConfig[index].peerId;
	printLog(log_data);
}
 /*
  * Update the  neigh bitfield
  * check if we have to send interested, if not sent already
  * 
  */
void proc_have(int index, 
			   int sockd) {
	int piece_number=0;
	//printf("In proc_have(), before reading piece_number\n");
	read (sockd, (void *)&piece_number, sizeof(int));
	//printf("In proc_have(), after reading piece_number\n");
	//printf("In proc_have() piece__number before ntohl : %d", piece_number);
	
	piece_number = ntohl(piece_number);
	
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = receiveHave;
	log_data.otherPeerId = peerConfig[index].peerId;
	log_data.pieceId = piece_number;
	printLog(log_data);
	
	//printf("In proc_have() piece__number after ntohl : %d index: %d", piece_number, index);
	update_neigh_bitfield(piece_number, index, neigh_bits[index]);		
	//printf("In proc_have(), after calling update_neigh_bitfield\n");
	if (check_interesting_piece(neigh_bits[index], piece_bits) ) {
		if (!sent_interested[index]) {
			sent_interested[index] = true;
			send_int(sockd);
		}
	}

}

 /*
  * Update neigh_bitfield accordingly
  * Using some bit operations, Find out if we have to send interested message
  * Int message is sent even if the peer has even one interesting piece(Present in peer, not present with us)
  * 
  */
void proc_bitfield(int msg_length, 
					int index,
					int sockd)
{	//printf("In Proc Bitfield\n");
	if (msg_length -1 == piece_bytes) {
		read (sockd, neigh_bits[index].bitfield_p, sizeof(uint8_t)*piece_bytes);
	} else {
		printf("proc_bitfield(): Invalid msglength for bitfield message\n");
	}
	print_bitfield(neigh_bits[index]);

	if (check_interesting_piece(neigh_bits[index], piece_bits)) {
		send_int(sockd);
	}
}
 /*
  * Read 4 bytes of piece_id from the buffer
  * We have to send the requested piece to the peer
  * 
  */
void proc_request(int msg_length, int index, int sockd) {
	int piece_id;
	read (sockd, (void *)&piece_id, sizeof(int));
	piece_id = ntohl(piece_id);
	int piece_size;
	uint8_t *piece_ptr;
	//printf("In Proc Request Msg_length: %d piece:id: %d \n", msg_length, piece_id);
	piece_ptr = calloc(c_config.pieceSize, sizeof(uint8_t));
	piece_size = get_piece(piece_ptr, piece_id);
	
	if (piece_size) {
		//if (sent_unchoke[index]) { 
		printf("Sending piece to peer ID: %d piece_size:%d \n", peerConfig[index].peerId, piece_size);
			send_piece(piece_id, piece_ptr, piece_size, sockd);
		//printf("After send_piece()\n");
		//}
	}
	free(piece_ptr);
}
 /*
  * Update piece_bits bitfield
  * send have messages to all the peers
  * send not interested to (if) any peers
  * Save the piece in the <peer-id> directory
  * Write information to log
  * Send another request as long we are unchoked
  * 
  */
int proc_piece(int msg_length, int index, int sockd) {
	int piece_id, i;
	read (sockd, (void *)&piece_id, sizeof(int));
	//printf("Before ntohl Piece id = %d sockd: %d\n", piece_id, sockd);

    piece_id = ntohl(piece_id);
   // printf("After ntohl Piece id = %d\n", piece_id);
	
	

	//Send not-interested
	
	uint8_t *payload = calloc(msg_length-1, sizeof(uint8_t));
	
	read (sockd, (void *)payload, msg_length-5);
	//printf("After Read message from socket\n");
	//printf("%x",payload);
	save_piece(payload, piece_id, msg_length-5);
	piece_received++;
	//printf("After save_piece()\n");
	
	/*Fill logging information*/
	log_data_t log_data;
	log_data.logType = downloadPiece;
	log_data.otherPeerId = peerConfig[index].peerId;
	log_data.pieceId = piece_id;
	printLog(log_data);
	
	update_bitfield(piece_id, piece_bits.bitfield_p); 
	//print_bitfield(piece_bits);	
	//printf("After update_bitfield ()\n");
	send_have_to_all(piece_id);
	//printf("After send_have_to_all() \n");
	
	
	if (piece_received < piece_count) {
		//printf("Pieces received : %d\n", piece_received);
		piece_id = random_interesting_piece(neigh_bits[index], requested_bits);
		if (piece_id > -1 && is_unchoked[index]) {
			send_request(piece_id, sockd);
		}
		return 0;
	} else {
		//Send not_interested to all the peers
		double cpu_time_used;
		end = clock();
		cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
		printf("fun() took %f seconds to execute \n", cpu_time_used);
		
		merge_file();
		DONE_DOWNLOADING = 1;
		/*Fill logging information*/
		log_data_t log_data;
		log_data.logType = downloadComplete;
		printLog(log_data);
		
		//Broadcast not interested
		for (i=0;i<totalNeighbours; i++) {
			send_not_int(sock_index[i]);
		}
		printf("\nIn else merge");
		printf("Pieces received : %d\n", piece_received);
		return 1;
	}
	free(payload);

}


/*
 * Take an action on incoming message accordingly
 * 
 */
int demux_msg(msg_header_t *msg_header, int sockd, uint32_t index) {
	/*
	 * Have processing functions for each msg type
	 * msg_length includes one byte message_type and excludes 4 byte msg_length field itself
	 * choke, unchoke, interested and not_interested have zero payload. No need of reading more.
	 * Have and reqeust messages have 4B payload
	 * Bitfield has piece_bytes number of bytes
	 * Piece has msg_length -1 number of bytes.
	 */
	  char buf[1000];
		 int count;
	 msg_header->msg_length = ntohl(msg_header->msg_length);
	 //printf("In demux msg: Msg Length:%x  sizeof msg_header: %d\n", msg_header->msg_length, sizeof(msg_header_t));
	switch(msg_header->msg_type) {
		case choke:
			printf("Choke\n");
			if (msg_header->msg_length != 1) {
				printf("Choke. Invalid length\n");
				abort();
			}
			proc_choke(index);
		break;
		case unchoke:
			printf("UnChoke\n");
			if (msg_header->msg_length != 1) {
				printf("UnChoke. Invalid length\n");
				abort();
			}
			proc_unchoke(index, sockd);

		break;
		case interested:
			//printf("Interested\n");
			if (msg_header->msg_length != 1) {
				printf("UnChoke. Invalid length\n");
				//abort();
			}
			proc_interested(index, sockd);
			
		break;
		case not_interested:
			printf("Not Interested\n");
			if (msg_header->msg_length != 1) {
				printf("Not Interested. Invalid length\n");
				//abort();
			}
			proc_not_interested(index);
			
		break;
		case have:
			//printf("Have\n");
			if (msg_header->msg_length != 5) {
				//~ printf("Have. Invalid length\n");
				//~ abort();
			}
			
			proc_have(index, sockd);
		break;
		case bit_field:
			//printf("Bitfield\n");
			if (msg_header->msg_length != piece_bytes+1) {
				printf("Bitfield. Invalid length\n");
				abort();
			}
			proc_bitfield(msg_header->msg_length, index, sockd);
		break;
		case request:
			//printf("Request\n");
			if (msg_header->msg_length !=  5) {
				printf("Request. Invalid length\n");
				abort();
			}
			proc_request(msg_header->msg_length, index, sockd);
		break;
		case piece:
			//printf("Piece\n");
			/*
			if (msg_header->msg_length < c_config.pieceSize + 5) {
				printf("Piece. Invalid length\n");
				abort();
			}
			*/
			return proc_piece(msg_header->msg_length, index, sockd);
		break;
		 default:
		
		 printf("Default\n");
		 	while (1)
                {
				count = read (sockd, (void *)&buf[0], 1000);
                  //write(1,buf,sizeof(buf));
                  if (count == -1 || count == 0)
                    {
                     break;
                    }
				}
//		 abort();
		break;
	}
	return 0;
}


/*Logging Utils*/
void printLog(log_data_t log_data)
{
	FILE *logFile;
	logFile = fopen(logFileName,"a+");
	char curTime[150];
	switch(log_data.logType){
		
		case connects:
			printf("I am in connects");
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] makes a connection to Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s\n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);				
		break;
		
		case receiveConnects:
			printf("I am in receive connects");
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] is connected from Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);
		break;
		
		case neighbourChange:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] has the preferred neighbour Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);
		break;
		
		case optUnchokedChange:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] has optimistically unchoked [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);
		break;
		
		case unchoking:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] is unchoked by Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);		
		break;
		
		case choking:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] is choked by Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId);
			printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);		
		break;
		
		case receiveHave:
		//~ //	printf("I am in receive have");
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] received the have message from Peer [%d] for piece index [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId, log_data.pieceId);
		//~ //	printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);
		break;
		
		case receiveInterest:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] received the 'interested' message from Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId, log_data.pieceId);
		//~ //	printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);			
		break;
		
		case receiveNoInterest:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] received the 'not interested' message from Peer [%d].\0", thisPeerConfig.peerId, log_data.otherPeerId, log_data.pieceId);
		//~ //	printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);		
		break;
		
		case downloadPiece:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] has downloaded the piece [%d] from Peer [%d].\0", thisPeerConfig.peerId, log_data.pieceId, log_data.otherPeerId);
		//~ //	printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);		
		break;
		
		case downloadComplete:
			memset(curTime,'\0',150);
			curTime[0]='[';
			getTime(curTime+1);
			*(curTime+strlen(curTime)-1) = '\0';
			sprintf(curTime+strlen(curTime), "]: Peer [%d] has downloaded the complete file.\0", thisPeerConfig.peerId);
		//~ //	printf("%s \n", curTime);
			//sleep(5);
			fprintf(logFile,"%s\n",curTime);
			fclose(logFile);		
		break;
		
		default:
		break;
	}	
}

void getTime(char currentTime[]){
		time_t rawtime;
		struct tm * timeinfo;

		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strcpy(currentTime, asctime (timeinfo));
		//~ *(currentTime+strlen(currentTime-2)) = '\0';
		//printf ( "Current local time and date: %s", asctime (timeinfo) );
	}
