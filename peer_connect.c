#include"header.h"
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

/* this function is run by the ConnectSender thread*/
void *send_connects(void *connect_data)
{

	int *peer_id = (int *)connect_data;
	handshake_msg_t temp_handshake;
	fill_handshake(&temp_handshake, *peer_id);


	int portno, n,i;
    struct sockaddr_in serv_addr;
    struct hostent *server;
	char buffer[256];
	//Send connection request to the peers whose peer_id is less than mine
	for(i=0; i<less_count; i++) {
		printf("Trying to connect to peer id with port %d\n",peerConfig[i].peerPort);
	 	//temp port no : @todo change later
		portno = peerConfig[i].peerPort;
		sockfd[i] = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd[i] < 0) {
			printf("ERROR opening socket\n");
		}
		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		//Change this to one given in peer config file
		
		printf("My Peer Address : %ul\n",inet_addr(peerConfig[i].peerAddr));
		
		serv_addr.sin_addr.s_addr = inet_addr(peerConfig[i].peerAddr);
		serv_addr.sin_port = htons(portno);
	 
		if (connect(sockfd[i],(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
	        printf("ERROR connecting\n");
	        return 0;
		}
		printf("Sending Handshake message\n");
		fill_handshake(&temp_handshake, thisPeerConfig.peerId);
		n = write(sockfd[i],&temp_handshake,sizeof(handshake_msg_t));

		printf("send_connects() peer_id %d\n", *peer_id);

		/*Fill logging information*/
		log_data_t log_data;
		log_data.logType = connects;
		printf("%d is logType\n",log_data.logType);
		log_data.otherPeerId = peerConfig[i].peerId;
		printf("%d is otherPeerId\n",peerConfig[i].peerId);
		printLog(log_data);
	}

		
	return 0;

}


/* 
 * This function is run by the ConnectReceiver thread 
 * Listens on the incoming connections
 * Accepts them and creates thread for each and every valid connection (Handshake validation)
 * 
 */
void *accept_connects(void *accept_data)
{
	struct sockaddr_in serv_addr;
	int temp_sockfd,portno, read_status;
	incoming_peer temp_clientdata;
	char buffer[256];
	int inc=0;
	handshake_msg_t temp_handshake;
	printf("Init Socket\n");
	temp_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	 
	if (temp_sockfd < 0) {
	    printf("ERROR opening the socket\n");
	}
	 
	printf("Initialise Server address\n");
	     bzero((char *) &serv_addr, sizeof(serv_addr));
	     
	     portno = thisPeerConfig.thisPeerPort;
	     serv_addr.sin_family = AF_INET;
	     
	     //Read IP address from config
	     printf("My IP Address : %ul\n",inet_addr(thisPeerConfig.thisPeerAddr));
	     serv_addr.sin_addr.s_addr = inet_addr(thisPeerConfig.thisPeerAddr);
	     serv_addr.sin_port = htons(portno);
	 
	     printf("Bind server address\n");
	     if (bind(temp_sockfd, (struct sockaddr *) &serv_addr,
	              sizeof(serv_addr)) < 0)
	              printf("ERROR on binding\n");
	 
	     printf("Start Listening for connections\n");
	     listen(temp_sockfd,5);
	     temp_clientdata.clilen = sizeof(temp_clientdata.cli_addr);
	     
	     //Todo change this to total number of peers - less_count
	 while(inc < (totalNeighbours - less_count)) {
	     temp_clientdata.newsockfd = accept(temp_sockfd,
										    (struct sockaddr *) &temp_clientdata.cli_addr,
											&temp_clientdata.clilen);
		 sockfd[inc+less_count] = temp_clientdata.newsockfd;
	 	 if (temp_clientdata.newsockfd < 0) {
	         printf("ERROR on accept\n");
		 }
	 	 printf("Waiting for Handshake message from the incoming peer connection\n");
		 bzero(buffer,256);
	
	     read_status = read(temp_clientdata.newsockfd, &temp_handshake, sizeof(handshake_msg_t));
	     
	     print_handshake(&temp_handshake);
	     
	      /*Fill logging information*/
		 log_data_t log_data;
		 log_data.logType = receiveConnects;
		 printf("%d is logType\n",log_data.logType);
		 log_data.otherPeerId = peerConfig[inc+less_count].peerId;
		 printf("%d is otherPeerId\n",peerConfig[inc].peerId);
		 printLog(log_data);
		
	     if (read_status < 0) {
	         printf("ERROR reading from socket; Could not get handshake message\n");
		 }
	    inc++;      
	   }       
	     
	int *peer_id = (int *)accept_data;

	printf("peer_id %d\n", *peer_id);

/* the function must return something - NULL will do */
return 0;

}
int  interested_count() {
	int count,i;
	for(i=0; i< totalNeighbours; i++) {
		if (is_interested[i]) {count++;
		}
	}
	return count;
	
}


int  unchoked_peers_count() {
	int count,i;
	for(i=0; i< totalNeighbours; i++) {
		if (sent_unchoke[i]) {count++;
		}
	}
	return count;
	
}
void timer_handler (int signum)
{
		int max_pref_neigh,i;
		int interested_peers = 0;

	 bool cond = false;
	printf ("timer expired \n");
	
	// If no peer is interested. Dont send Choke/Unchoke
	for(i=0; i< totalNeighbours; i++) {
		if (is_interested[i]) {
			interested_peers++;
			cond =true;
		}
	}
	if (!cond) {
		return;
	}
	//Check total number of interested peers. If that is less than preferred neighbours, 
	//Unchoke total number of interested peers.
	//Simplification: ALL peers are interested. Todo: more
	max_pref_neigh = MIN(c_config.prefNeighbours, interested_peers);
	//max_pref_neigh = c_config.prefNeighbours;
	i=0;
	
	//Round robin unchoking 
	while(i< max_pref_neigh) {
			count++;
		 pref_neigh[i] = count%totalNeighbours;
		 printf("Preferred neighbour %d peer ID : %d", i, peerConfig[pref_neigh[i]].peerId);
		 
		 /*Fill logging information*/
		 log_data_t log_data;
		 log_data.logType = neighbourChange;
		 printf("%d is logType\n",log_data.logType);
		 log_data.otherPeerId = peerConfig[pref_neigh[i]].peerId;
		 printf("%d is otherPeerId\n",peerConfig[pref_neigh[i]].peerId);
		 printLog(log_data);
		 
		 if (is_interested[pref_neigh[i]]) {i++;}
		 //i++;
		 
	}
	int j;
	for(i=0; i< totalNeighbours; i++) {
		cond = false;
		//Check if i is presnt in preferred neighbour arrray
		 for (j=0; j<max_pref_neigh;j++) {
				if (pref_neigh[j] == i) {
					cond = true;
					break;
				} else {
					cond = false;
				}
			}
		 if (cond) {
			 //If already unchoked, do nothing. else send unchoke
			 
			 if (!sent_unchoke[i]) {
				send_unchoke(sockfd[i]);
				sent_unchoke[i] = true;
			}
		} else {
			//Not a prefererd neighbour. If unchoked before, choke now
			if (sent_unchoke[i]) {
				send_choke(sockfd[i]);
				sent_unchoke[i] = false;
			}
		}
	}
}

/*
 * Thread to choke/unchoke periodically
 * 
 * 
 */
void *timer_thread(void *data)
{
	struct sigaction sa;
	struct itimerval timer;

 /* Install timer_handler as the signal handler for SIGVTALRM. */
 memset (&sa, 0, sizeof (sa));
 sa.sa_handler = &timer_handler;
 sigaction (SIGVTALRM, &sa, NULL);

 timer.it_value.tv_sec = 0;
 timer.it_value.tv_usec = 5000;
 
 timer.it_interval.tv_sec = c_config.unchokeInt;
 timer.it_interval.tv_usec = 0000;
 
 setitimer (ITIMER_VIRTUAL, &timer, NULL);
 while(1) {
	// if (DONE_DOWNLOADING) {return;}
 }
	
}


void opt_unchoke_handler (int signum)
{
	int i=0,j=0;
		printf("Optmistically unchoking\n");
int unchoked_peer_count = unchoked_peers_count();
int interested_peer_count = interested_count();


	if (interested_peer_count > unchoked_peers_count) {
		while (i < totalNeighbours) {
		count++;
		j = count%totalNeighbours;
			if(!sent_unchoke[j] && is_interested[j]) {
				send_unchoke(sockfd[j]);
				sent_unchoke[j] = true;
				log_data_t log_data;
				log_data.logType = optUnchokedChange;
				//printf("%d is logType\n",log_data.logType);
				log_data.otherPeerId = peerConfig[j].peerId;
				//printf("%d is otherPeerId\n",peerConfig[pref_neigh[i]].peerId);
				printLog(log_data);
				printf("Optimistically unchoking Peer:\n", peerConfig[j].peerId);
				if (sent_unchoke[opt_neighbour]) {
					send_choke(sockfd[opt_neighbour]);
					sent_unchoke[opt_neighbour] = false;
					printf("choking Peer:\n", peerConfig[opt_neighbour].peerId);
				}
				opt_neighbour = j;
				break;
			}
	i++;
		}
	}
}
void *opt_unchoke(void *data)
{
	struct sigaction sa;
	struct itimerval timer;

 /* Install timer_handler as the signal handler for SIGVTALRM. */
 memset (&sa, 0, sizeof (sa));
 sa.sa_handler = &opt_unchoke_handler;
 sigaction (SIGVTALRM, &sa, NULL);

 timer.it_value.tv_sec = 1;
 timer.it_value.tv_usec = 00;
 
 timer.it_interval.tv_sec = c_config.optunchokeInt;
 timer.it_interval.tv_usec = 6000;
 
 setitimer (ITIMER_VIRTUAL, &timer, NULL);
 while(1) {
	// if (DONE_DOWNLOADING) {return;}
 }
	
}


/*
 * Thread to receive messages from peers. Planning to use epoll 
 * to poll on all the sockets.
 */
void *receive_messages(void *data){
	   start = clock();
     
	struct epoll_event event;
	struct epoll_event *events;
	int done = 0;
	int efd = epoll_create1 (0);
	if (efd == -1) {
      perror ("epoll_create");
      //abort ();
    }
    int i;
	for (i=0;i<MAX_PEERS-1;i++) {
		event.data.fd = sockfd[i];
		//event.data.u32 = i;
		event.events = EPOLLIN;
		int s = epoll_ctl (efd, EPOLL_CTL_ADD, sockfd[i], &event);
		printf("sockfd[%d]:%d\n", i,sockfd[i]);
		if (s == -1)
		{
			perror ("epoll_ctl");
			//abort ();
		}
	}

 
  /* Buffer where events are returned */
  events = calloc (MAXEVENTS, sizeof event);
	
  /* The event loop */
  while (1)
    {
      int n, i;
	//printf("Waiting for epoll_wait to return\n");
      n = epoll_wait (efd, events, MAXEVENTS, -1);
     // printf("Number of events:%d\n", n);
      for (i = 0; i < n; i++){
		if ((events[i].events & EPOLLERR) ||
              (events[i].events & EPOLLHUP) ||
              (!(events[i].events & EPOLLIN))) {
              /* An error has occured on this fd, or the socket is not
                 ready for reading (why were we notified then?) */
	      fprintf (stderr, "epoll error\n");
	      close (events[i].data.fd);
	      continue;
	    } else if (events[i].events & EPOLLIN) {
			     //  printf("Read event detected\n");

                  ssize_t count;
                  char buf[1000];
				  msg_header_t msg_header;
				  count = read (events[i].data.fd, (void *)&msg_header, sizeof(msg_header_t));
                  
                  //printf("Calling demux_msg\n");
                  int success = demux_msg(&msg_header,events[i].data.fd, sock_index[events[i].data.fd]);
                  
                  
                  if (success) {
					  //return;
				  }
                  //Read anything extra
                  
                   /* If errno == EAGAIN, that means we have read all
                    data. So go back to the main loop. */
                      if (errno != EAGAIN)
                        {
                         // perror ("read");
                          //printf("We should have read entire packet. This sholdnt happen\n");
                        
                        }
			/*while (1)
                {
				count = read (events[i].data.fd, (void *)&buf[0], 1000);
                  //write(1,buf,sizeof(buf));
                  if (count == -1 || count == 0)
                    {
                 
                     break;
                    }
				}*/
				
		}
	}
}
  
}
/*
 * Fill Handshake Message
 */
void fill_handshake(handshake_msg_t *hs_msg,	int peer_id) 
{
	printf("---Filling handshake header message---\n");
	strncpy(hs_msg->hs_hdr,"P2PFILESHARINGPROJ",18);
	strncpy(hs_msg->zero_bits,"1111111111",10);
	hs_msg->peer_id = peer_id;
}


/* 
 * Copies n characters from alphabets starting at random position
 * this is a helper function for generating data file
 */
int copy_characters(char *buffer, int n) 
{
	
        char alphabets[27] = "abcdefghijklmnopqrstuvwxyz";
        int rand_int = rand()%(26-n);
		strncpy(buffer, (const char * __restrict__)(&alphabets)+rand_int, n);
	//printf("%s", buffer);
}


/*
 * create a file with specified name and given size(Bytes)
 * Write something random into file
 */
int write_file(unsigned long long size, char *filename) 
{
	
	unsigned long long counter, remaining;
	char buffer[1000];
	FILE *data_file;
	data_file = fopen(filename,"w");
	if (!data_file) {
		printf("write_file() : Unable to write into data file!\n");
		return 0;
	}
	copy_characters(buffer,1000);
	for (counter=0; counter < size/1000; counter++) {
		 fseek(data_file,1000*counter,SEEK_SET);
		 
		 fwrite(&buffer, 1000, 1, data_file);
	}
	remaining = size%1000;
	
	fseek(data_file,10*(counter),SEEK_SET);
	copy_characters(buffer,remaining);
	fwrite(&buffer, remaining, 1, data_file);
	 fclose(data_file);
	return 1;
}


/*
 * Given a socket descriptor, makes it non-blocking
 * Socket should be non-blocking to be used by epoll
 */
static int
make_socket_non_blocking (int sfd)
{
  int flags, s;

  flags = fcntl (sfd, F_GETFL, 0);
  if (flags == -1)
    {
      perror ("fcntl");
      return -1;
    }

  flags |= O_NONBLOCK;
  s = fcntl (sfd, F_SETFL, flags);
  if (s == -1)
    {
      perror ("fcntl");
      return -1;
    }

  return 0;
}
/*
 * Make all the sockets non-blocking
 */
 int
make_all_sockets_nb ()
{ 	int i;
	for (i=0; i< MAX_PEERS-1; i++) {
		make_socket_non_blocking(sockfd[i]);
	}
	return 0;
}
