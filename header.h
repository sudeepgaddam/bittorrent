#ifndef HEADER
#define HEADER
/**************************
*MACROS
**************************/
#define PREFNEIGH "NumberOfPreferredNeighbours"
#define COMCFG "Common.cfg"
#define PEERCFG "PeerInfo.cfg"
#define MAX_PEERS 5
#define HANDSHAKE_SIZE 32
#define MAXEVENTS 64
#define MAX_SOCKET_RANGE 100
#define MAX_PIECES 500
/*************************
*INCLUDE FILES
**************************/

#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<errno.h>
#include<pthread.h>
#include<stdbool.h>
#include "include/hdr_utils.h"
#include<time.h>
#include <sys/time.h>
#include <signal.h>

#define CCONFPARAMS 6
#define CHARACTERCOPY 10

/*************************
* GLOBAL VARIABLES
**************************/
typedef struct cConfig
{
  int   prefNeighbours;
  int   unchokeInt;
  int   optunchokeInt;
  int   fileSize;
  int   pieceSize;
  char  fileName[255];
}common_config;

typedef struct pConfig
{
int peerId;
char peerAddr[100];
int peerPort;
int peerHasFile;
}peer_config_t;

typedef struct thisPConfig
{
	int peerId;
char thisPeerAddr[100];
int thisPeerPort;
int thisPeerHasFile;
}this_peer_config;


typedef struct handshake_msg_s {
	char hs_hdr[18]; //18 bytes Ex: P2PFILESHARINGPROJ
	char zero_bits[10]; 
	int peer_id;  // Peer id of participating peer
}handshake_msg_t;



typedef struct user_info
{
    socklen_t clilen;
    struct sockaddr_in cli_addr;
    int newsockfd;
    char username[16];
}incoming_peer;
/*
 * Bitfields are used to store whether piece is present/not. 
 * Number of pieces is variable. Hence, we can't  fixed number of bits.
 * We will have a uint8 pointer that points to our bits.
 */
typedef struct bitfield
{
	uint8_t *bitfield_p;
}bitfield_t;


typedef struct __attribute__((__packed__)) msg_header_s {
	int msg_length;
	uint8_t msg_type;
}msg_header_t;
/* 
 * struct used for messages with piece_id
 */
typedef struct __attribute__((__packed__)) piece_id_msg_s {
	msg_header_t msg_header;
	int piece_id
}piece_id_msg_t;

/*
 * struct used for updating logfile
 */


typedef struct actual_msg_s {
	msg_header_t msg_header;
	void *payload;
}actual_msg_t;



typedef enum message_type {
		choke, //0
		unchoke, //1
		interested, //2
		not_interested, //3
		have, //4
		bit_field, //5
		request, //6
		piece //7
		
}message_types;

typedef enum log_type {
	connects, //0
	receiveConnects, //1
	neighbourChange, //2
	optUnchokedChange, //3
	unchoking, //4
	choking, //5
	receiveHave, //6
	receiveInterest, //7
	receiveNoInterest, //8
	downloadPiece, //9
	downloadComplete, //10
	
}log_types;

typedef struct log_data_s{
	int otherPeerId;
	int prefNeighbours[10];
	int pieceId;
	int existingPieces;
	uint8_t logType;
}log_data_t; 

/*************************
* GLOBAL Declarations
**************************/
	     clock_t start, end;

common_config c_config;
peer_config_t peerConfig[MAX_PEERS];
this_peer_config thisPeerConfig;

//Global for now; todo: Change in future
incoming_peer user[MAX_PEERS];

int less_count;
int totalNeighbours;
int piece_count;
int piece_received;

//Socket descriptors are globals
int sockfd[MAX_PEERS-1];
//index is socket descriptor, value is index, used to dereference arrays(Neigh bitfield etc)
int sock_index[MAX_SOCKET_RANGE]; 
//is_interested[index] = true means index peer is interested. 
bool is_interested[MAX_PEERS];

bool is_unchoked[MAX_PEERS];
//is set if Sent unchoked to the peer_id
bool sent_unchoke[MAX_PEERS];

bool sent_interested[MAX_PEERS];
int piece_bytes;
//char alphabets[27] = "abcdefghijklmnopqrstuvwxyz";
bitfield_t piece_bits;
//Array of pointers to neghbours bitfields
bitfield_t neigh_bits[MAX_PEERS-1];
//sets piece_bits and requested piece bits
bitfield_t requested_bits;

int pref_neigh[MAX_PEERS];
/*
pthread_cond_t producerVar;
pthread_cond_t haveVar;
pthread_cond_t interestedVar;
pthread_cond_t notIntVar;
pthread_cond_t chokeVar;
pthread_cond_t unchokeVar;
pthread_cond_t pieceVar;
pthread_cond_t requestVar;
pthread_cond_t bitfieldVar;
*/
message_types new_message;
log_types new_log;

int *rand_array;

//Folder name for peer
char folderName[30];  
char tempFolderName[30];
//Log File name for peer
char logFileName[30];

int DONE_DOWNLOADING;
static int count;

static int opt_neighbour;
/***************************
* FUNCTION DECLARATIONS
***************************/

int readCConfig(common_config *commonConfig);
int read_peer_config(int peerId);
void *send_connects(void *connect_data);
void *accept_connects(void *accept_data);
void *opt_unchoke(void *data);

void fill_handshake(handshake_msg_t *hs_msg, int peer_id); 
int copy_characters(char *buffer, int n); 
int write_file(unsigned long long size, char *filename); 
void print_config(common_config *conf);
void print_peer_config(peer_config_t *peer_config /*,int totalNeighbours*/);
void print_handshake(handshake_msg_t *hs_msg);
//New ones
void *timer_thread(void *connect_data);
void *receive_messages(void *accept_data);
void demux_rcv_msg();
void send_bitfieldmsg();
int get_piece_count();
int make_all_sockets_nb ();
//Bitfield related @todo: move to another header file
void set_bitfield(int piece_bytes);
void unset_bitfield(int piece_bytes);
void make_bitfield(int piece_bytes);
void update_bitfield(int piece_number,uint8_t *bitfieldp);

void print_bitfield(bitfield_t bf);
void update_neigh_bitfield(int piece_number,
							int peersd, 
							bitfield_t neigh_bits);
void alloc_neighbits(int total_neighbours,
					 int piece_bytes,
					 bitfield_t *neigh_bits);
//msg_handlers related @todo: move to another header file
int demux_msg(msg_header_t *msg_header, int sockd, uint32_t index);
void proc_choke(int index);
void proc_unchoke(int index, int sockd);
void proc_interested(int index, int sockd);
void proc_not_interested(int index);
void proc_have();
void proc_bitfield(int msg_length, 
					int index,
					int sockd);
void proc_request(int msg_length, int index, int sockd);
int proc_piece(int msg_length, int index, int sockd);

//send_threads.c related @todo: move to another header file
void send_have(int sockd, int piece_id);
void send_int(int int_data);
void send_not_int(int not_int_data);
void send_choke(int choke_data);
void send_unchoke(int unchoke_data);
void send_piece(int piece_id, uint8_t *piece_ptr, int piece_size, int sockd);
void send_request(int piece_id, int sockd);
void send_bitfield(int bitfield_data);
void update_bitfield(int piece_number,uint8_t *bitfieldp); 
//For Random Shuffle
void randomize( int *arr, int n );

//Piece related
void save_piece(uint8_t *piece_ptr, int piece_id, int piece_size);
int get_piece(uint8_t *piece_ptr, int piece_id);
void split_file(char fileName[]);

//Logfile related
void printLog(log_data_t log_data);
void getTime(char currentTime[]);
#endif
