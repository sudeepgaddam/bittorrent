#include "header.h"

void save_piece(uint8_t *piece_ptr, int piece_id, int piece_size) {
	
	char temp[30];
	char file_name[10];
	strcpy(temp, tempFolderName);
	sprintf(file_name, "/%d.tmp", piece_id);
	strcat(temp, file_name);
	//printf("File Paths is %s\n",temp);
	
	FILE *pieceFile;
	int write_length;
	printf("In save piece %s\n", temp);
	pieceFile = fopen(temp,"w");

	 if(NULL != pieceFile) {
		//printf("File Opened for write \n");
		write_length =  fwrite((void *)piece_ptr, sizeof(uint8_t), piece_size, pieceFile);
		if (write_length != piece_size) {
			printf("Write error in save_piece()\n");
		}
	}
	fclose(pieceFile);
}
/*
 * 
 */
int get_piece(uint8_t *piece_ptr, int piece_id) {
		if (piece_id >= piece_count) {
			return 0;
		}
	char temp[30];
	char file_name[10];
	strcpy(temp, tempFolderName);
	sprintf(file_name, "/%d.tmp", piece_id);
	strcat(temp, file_name);
		printf("In get piece %s\n", temp);
		
	//printf("file name: %s piece-id:%d\n",temp, piece_id);
	FILE *pieceFile;
	int read_length;
	pieceFile = fopen(temp,"r");
	 if(NULL != pieceFile) {
		//printf("File Opened for reading\n");
		read_length =  fread((void *)piece_ptr, sizeof(uint8_t), c_config.pieceSize, pieceFile);
	}
	fclose(pieceFile);
	return read_length;

}

void split_file(char fileName[])
{
	FILE *pieceFile;
	FILE *mainFile;
	
	mainFile = fopen(fileName,"r");
	
	char temp[30];
	char file_name[10];
	int read_length, write_length, i;
	strcpy(temp, folderName);
	uint8_t *piece_ptr;
	piece_ptr = calloc(c_config.pieceSize, sizeof(uint8_t));
	for (i=0; i< piece_count; i++) {
		strcpy(temp, tempFolderName);
		sprintf(file_name, "/%d.tmp", i);
		strcat(temp, file_name);	
		pieceFile = fopen(temp,"w");
		read_length =  fread((void *)piece_ptr, sizeof(uint8_t), c_config.pieceSize, mainFile);
		write_length =  fwrite((void *)piece_ptr, sizeof(uint8_t), read_length, pieceFile);
		fclose(pieceFile);
	}
	free(piece_ptr);	
	fclose(mainFile);
}

void merge_file()
{
	FILE *pieceFile;
	FILE *mainFile;
	
	char temp[30];
	char file_name[10];
	char main_name[30];

	int read_length, write_length, i;
	int ret;
	//strcpy(main_name, folderName);
	sprintf(main_name,"%s/", folderName);
	strcat(main_name, c_config.fileName);
	printf("Main file name: %s \n",main_name);

	mainFile = fopen(main_name,"w");
	
	uint8_t *piece_ptr;
	piece_ptr = calloc(c_config.pieceSize, sizeof(uint8_t));
	
	for (i=0; i< piece_count; i++) {
		strcpy(temp, tempFolderName);
		sprintf(file_name, "/%d.tmp", i);
		strcat(temp, file_name);	
			//printf("Temp file name: %s \n",temp);
		pieceFile = fopen(temp,"r");
		
		read_length =  fread((void *)piece_ptr, sizeof(uint8_t), c_config.pieceSize, pieceFile);
		write_length =  fwrite((void *)piece_ptr, sizeof(uint8_t), read_length, mainFile);
		
		fclose(pieceFile);
		//ret = remove(temp);
		 //~ if(ret == 0) {
			//~ printf("File deleted successfully");
		//~ }
		//~ else {
			//~ printf("Error: unable to delete the file");
		//~ }
	}
	free(piece_ptr);
	fclose(mainFile);
}
