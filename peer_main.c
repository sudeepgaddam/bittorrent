#include"header.h"

void fill_sock_index(){
	int i;
	for (i=0;i < MAX_SOCKET_RANGE; i++){
		sock_index[i] = -1;
	}
	for (i=0;i<totalNeighbours; i++) {
		
		sock_index[sockfd[i]] = i;
		//printf("sock_index[sockfd[i]] = %d sockfd[i] = %d\n", sock_index[sockfd[i]], sockfd[i]);
	}
}

void 	init_is_interested() {
	int i;
	for (i=0;i<totalNeighbours; i++) {
		is_interested[i] = false;
		pref_neigh[i] = -1;
		sent_unchoke[i] = false;
		sent_interested[i] = false;
	}	
}
static void init_rand_array() {
	int i;
	rand_array = calloc(piece_bytes, sizeof(int));
	for (i=0;i<piece_bytes;i++) {
		*(rand_array + i) = i;
	}
}
static void test_bitfields(){
	int i,j;
			 update_bitfield(122, piece_bits.bitfield_p);
		update_neigh_bitfield(230, 0,neigh_bits[1]);
		i = random_interesting_piece(neigh_bits[1], piece_bits);
	printf("Interesting piece ID%d\n", i);
/*
	for (j=0; j< piece_count; j++) {
		 update_bitfield(j, piece_bits.bitfield_p);
		 print_bitfield(piece_bits);
	}
	for (i=0; i<totalNeighbours; i++) {
		for (j=0; j< piece_count; j++) {
		update_neigh_bitfield(j, 0,neigh_bits[i]);
		print_bitfield(neigh_bits[i]);
		}
	}
	* */
}
/*************************
* MAIN
**************************/
int main(int argc, char *argv[])
{	
	pthread_t ConnectSender;
	pthread_t ConnectReceiver;
	/*
	pthread_cond_init (&producerVar, NULL);	
	pthread_cond_init (&haveVar, NULL);	
  	pthread_cond_init (&interestedVar, NULL);	
  	pthread_cond_init (&notIntVar, NULL);	
  	pthread_cond_init (&chokeVar, NULL);	
  	pthread_cond_init (&unchokeVar, NULL);	
  	pthread_cond_init (&pieceVar, NULL);	
  	pthread_cond_init (&requestVar, NULL);	
  	pthread_cond_init (&bitfieldVar, NULL);	
    */
    if(argc != 2) {
       printf("Invalid Number of arguments\n");
       return 0;
    } 
    
    int peerId = atoi(argv[1]);
    
    if (peerId <= 0) {
		printf("Invalid Peer ID, Please specify a right one\n");
		return 0;
	}
	
	if (!readCConfig(&c_config)) {
		printf("Not able to read common cfg file\n");
		return 0;
	}
	print_config(&c_config);
	
	
	//Get the total piece count
	piece_count = get_piece_count();
	piece_bytes = (piece_count%8 == 0) ? piece_count/8: (piece_count/8)+1;
	piece_bits.bitfield_p = calloc(piece_bytes, sizeof(uint8_t));
	requested_bits.bitfield_p = calloc(piece_bytes, sizeof(uint8_t));
	if (!read_peer_config(peerId)) {
		printf("Not able to read peer cfg file\n");
		return 0;
		
	}
	
	//Create the peer directory if it is not already present
     makeDir();
    //~ FILE *logFile;
	//~ logFile = fopen(logFileName,"w+");
	//~ fclose(logFile);
	print_peer_config(peerConfig);
	init_rand_array();
	make_bitfield(piece_bytes);
	if (thisPeerConfig.thisPeerHasFile) {
			//write_file(c_config.fileSize, c_config.fileName);

		split_file(c_config.fileName);
	}
	//update_bitfield(311,piece_bits.bitfield_p);
	//print_bitfield(piece_bits);

	//Connect to the peers with lesser peerID --- create a thread (ConnectSender) 
	// Accept connnections from peers with greater peer ID  --- Create a thread (ConnectReceiver) 
	pthread_create (&ConnectSender, NULL, send_connects, &peerId);
	pthread_create (&ConnectReceiver, NULL, accept_connects, &peerId);
	//Get all the connections in global socket descriptors   
	pthread_join(ConnectSender, NULL);
	pthread_join(ConnectReceiver, NULL);      
	
	make_all_sockets_nb();
	alloc_neighbits(totalNeighbours, piece_bytes, &neigh_bits[0]);
	init_is_interested();
	if (thisPeerConfig.thisPeerHasFile) {
		int i;
		printf("I have file\n");
		for (i=0; i< totalNeighbours; i++) {
			send_bitfield(sockfd[i]);
		}
		
	} else {
		//Dont do anything
	}
	printf("Testing bit fields\n");
	//test_bitfields();
	fill_sock_index();
	//After all the connections are done setting up. File sharing starts. 
	//Bit torrent related messages sending and receiving starts
	//Create a thread for sending and another for receiving messages from peers.
DONE_DOWNLOADING = 0;
	pthread_t timerThread;
	pthread_t messageReceiver;

	pthread_t optUnchokeThread;
	/*
	pthread_t interestedSender;
	pthread_t notIntSender;
	pthread_t chokeSender;
	pthread_t unchokeSender;
	pthread_t pieceSender;
	pthread_t requestSender;
	pthread_t bitfieldSender;
	*/
	
	
    pthread_create (&messageReceiver, NULL, receive_messages, &peerId);
    pthread_create (&timerThread, NULL, timer_thread, &peerId);
    pthread_create (&optUnchokeThread, NULL, opt_unchoke, &peerId);
    //Create 8 threads for all kind of messages. These senders execute event based
    //using pthread_cond_signal. This approach is just one way of doing.
    //@todo : do it in a better way. using Array of function pointers and loop or something
    /*
    
    pthread_create (&interestedSender, NULL, send_int, &peerId);
    pthread_create (&notIntSender, NULL, send_not_int, &peerId);
    pthread_create (&chokeSender, NULL, send_choke, &peerId);
    pthread_create (&unchokeSender, NULL, send_unchoke, &peerId);
    pthread_create (&pieceSender, NULL, send_piece, &peerId);
    pthread_create (&requestSender, NULL, send_request, &peerId);
    pthread_create (&bitfieldSender, NULL, send_bitfield, &peerId);
    
    pthread_join(haveSender, NULL);
	pthread_join(interestedSender, NULL); 
	pthread_join(notIntSender, NULL);
	pthread_join(chokeSender, NULL);  
    pthread_join(unchokeSender, NULL);
	pthread_join(pieceSender, NULL);  
	pthread_join(requestSender, NULL);
	pthread_join(bitfieldSender, NULL);  
	*/
	pthread_join(optUnchokeThread, NULL);  
	pthread_join(timerThread, NULL);
	pthread_join(messageReceiver, NULL);      

}


