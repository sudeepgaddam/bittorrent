#include"header.h"

/****************
* Make Directory
****************/
int makeDir()
{
  strcpy(folderName, "peer_[");
  sprintf(folderName+6, "%d", thisPeerConfig.peerId);
  //itoa(thisPeerConfig.peerId, folderName+6, 10);
  strcat(folderName, "]");
  
  strcpy(tempFolderName, folderName);
  strcat(tempFolderName, "/temp");
  
  if(mkdir(folderName, 0777) == -1 || mkdir(tempFolderName, 0777) == -1)
  {
    return 0;
  }
  
  
  strcpy(logFileName, "log_peer_[");
  sprintf(logFileName+10,"%d", thisPeerConfig.peerId);
  strcat(logFileName,"].log");
  printf("logFileName : %s\n", logFileName);
  return 1;
}
/****************
* Get total number of pieces
****************/
int get_piece_count()
{
	if (c_config.fileSize%c_config.pieceSize) {
		return (c_config.fileSize/c_config.pieceSize)+1; 
	} else {
		return (c_config.fileSize/c_config.pieceSize);
	}
}
/****************************
* READ COMMON CONFIGURATION 
* - Reads PeerConfig file and extracts information from it 
****************************/

int readCConfig(common_config *commonConfig)
{
  printf("In read CConfig \n");
  char  stringBuffer[100];
  int   intBuffer;
  
  FILE *cConfFile;

  cConfFile = fopen(COMCFG,"r");
  if(NULL != cConfFile)
  {
    printf("File Opened\n");
    int i;
    
    for(i=1; i<=CCONFPARAMS; i++) {  
        if(fscanf(cConfFile, "%s", stringBuffer)) {
	
			if(0 == strcmp(stringBuffer,PREFNEIGH)) { 
				if (fscanf(cConfFile, "%d", &intBuffer))
				commonConfig->prefNeighbours = intBuffer;
			} else if(0 == strcmp(stringBuffer, "UnchokingInterval")) {
				if (fscanf(cConfFile, "%d", &intBuffer)) {
					commonConfig->unchokeInt = intBuffer;
				}
			} else if(0 == strcmp(stringBuffer, "OptimisticUnchokingInterval")) {
				if(fscanf(cConfFile, "%d", &intBuffer)) { 	  
					commonConfig->optunchokeInt = intBuffer;
				}
			} else if(0 == strcmp(stringBuffer, "FileSize")) {
				if(fscanf(cConfFile, "%d", &intBuffer)) { 	  
					commonConfig->fileSize = intBuffer;
				}
			} else if(0 == strcmp(stringBuffer, "PieceSize")) {
				if(fscanf(cConfFile, "%d", &intBuffer)) { 	  
					commonConfig->pieceSize = intBuffer;
				}
			} else if(0 == strcmp(stringBuffer, "FileName")) {
				if(fscanf(cConfFile, "%s", stringBuffer)) { 	  
					strcpy(commonConfig->fileName, stringBuffer);	      
				}
			}
		} else {
			printf("fscanf from string failed\n");
		}
      memset(stringBuffer,'\0',100); //clear buffer
    }    
  } else {
      printf("Common Config file doesn not exist\n");
      return 0;
  }
  
  fclose(cConfFile);
  return 1;
}


/****************************
* READ PEER CONFIGURATION  
* - Reads PeerConfig file and extracts information from it
****************************/

int read_peer_config(int peerId)
{
  int isPeerExist = 0;  
  int intBuffer;
  int temp;
  char stringBuffer[100];
  
  FILE *pConfFile;
  pConfFile = fopen(PEERCFG,"r");
  
  
  if(NULL == pConfFile)
  {	
	  printf("File opening Failed from path %s\n", PEERCFG);
	  return 0;
  }
    printf("PConfig file open\n");
    int i = 0;
    while (!feof(pConfFile)) {
		temp = fscanf(pConfFile,"%d", &intBuffer);
		if (intBuffer == peerId) {
		    less_count = i; //less_count number of peers have lesser peer id than current peer
            isPeerExist = 1;
            thisPeerConfig.peerId = intBuffer;
			temp = fscanf(pConfFile, "%s", stringBuffer);
			strcpy(thisPeerConfig.thisPeerAddr, stringBuffer);
			temp = fscanf(pConfFile, "%d", &thisPeerConfig.thisPeerPort);
			temp = fscanf(pConfFile, "%d", &thisPeerConfig.thisPeerHasFile);
		} else if (intBuffer){
			peerConfig[i].peerId = intBuffer;
		
			fscanf(pConfFile, "%s", stringBuffer);
			strcpy(peerConfig[i].peerAddr, stringBuffer);
	//Uncomment if hostnames are used			
/* 
struct addrinfo hints, *infoptr;
memset(&hints, 0, sizeof(struct addrinfo));			
hints.ai_family = AF_INET; // AF_INET means IPv4 only addresses
hints.ai_flags = AI_PASSIVE;
hints.ai_socktype = SOCK_STREAM;
int result = getaddrinfo(stringBuffer, NULL, &hints, &infoptr);
if (result) {
  fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
  //exit(1);
}

struct addrinfo *p;
char host[256],service[256];

for(p = infoptr; p != NULL; p = p->ai_next) {

  getnameinfo(p->ai_addr, p->ai_addrlen, host, sizeof(host), service, sizeof(service), NI_NUMERICHOST);
  puts(host);
}
strcpy(thisPeerConfig.thisPeerAddr, host);
freeaddrinfo(infoptr);		
*/	strcpy(peerConfig[i].peerAddr, stringBuffer);//comment if hostnames are used
			fscanf(pConfFile, "%d", &intBuffer);
			peerConfig[i].peerPort = intBuffer;

			fscanf(pConfFile, "%d", &intBuffer);
			peerConfig[i].peerHasFile = intBuffer;
			i++;
	    }
    }// end of file
    fclose(pConfFile); 
    if(isPeerExist == 1) {
       totalNeighbours = i;
       return 1;
    } else {
        printf("Peer is not listed in the config file\n");
        return 0;
    }
    return 1;
}
