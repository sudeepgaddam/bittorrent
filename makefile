CC = gcc -w
CFLAGS  = -g
RM = rm
INC_DIR = include/
INC1      = include/
INCDIRS   =  -I${INC1} 
CFLAGS    =  ${INCDIRS} 

# To create the executable file peer_test we need the object files
# peer_main.o, read_config.o, peer_connect.o and peer_PrintTrace.o:
#
peer_test:  peer_main.o read_config.o peer_connect.o peer_PrintTrace.o msg_handlers.o bitfield_utils.o send_threads.o randomise.o piece_utils.o header.h
	$(CC) $(CFLAGS) -o peer_test peer_main.o read_config.o peer_connect.o peer_PrintTrace.o msg_handlers.o bitfield_utils.o send_threads.o randomise.o piece_utils.o -lpthread

#To create the object file peer_PrintTrace.o, we need the source
# file peer_PrintTrace.c and header.h
#
peer_PrintTrace.o: peer_PrintTrace.c header.h
	$(CC) $(CFLAGS) -c peer_PrintTrace.c

# To create the object file peer_connect.o, we need the source
# files peer_connect.c and header.h
#
peer_connect.o:  peer_connect.c header.h
	$(CC) $(CFLAGS) -c peer_connect.c

# To create the object file read_config.o, we need the source files
# read_config.c and header.h:
#
read_config.o:  read_config.c header.h 
	$(CC) $(CFLAGS) -c read_config.c

# To create the object file peer_main.o, we need the source files
# peer_main.c and header.h:
#
peer_main.o:  peer_main.c header.h 
	$(CC) $(CFLAGS) -c peer_main.c

msg_handlers.o: msg_handlers.c header.h 
	$(CC) $(CFLAGS) -c msg_handlers.c
	
bitfield_utils.o: bitfield_utils.c header.h
	$(CC) $(CFLAGS) -c bitfield_utils.c
	
send_threads.o: send_threads.c header.h
	$(CC) $(CFLAGS) -c send_threads.c
	
randomise.o: randomise.c header.h
	$(CC) $(CFLAGS) -c randomise.c
	
piece_utils.o: piece_utils.c header.h
	$(CC) $(CFLAGS) -c piece_utils.c

# To start over from scratch, type 'make clean'.  This
# removes the executable file, as well as old .o object
# files and *~ backup files:
#
clean: 
	$(RM)  *.o peer_test
	$(RM) -rf peer_\[10*
	$(RM) -rf log_peer_\[10*
