#include"header.h"
/*
 * Print Common Config Parameters
 */
void print_config(common_config *conf) {
	printf(" *** Printing Common config parameters *** \n");
	printf("Preferred Neighbours : %d\n", conf->prefNeighbours);
	printf("Unchoke Int : %d\n", conf->unchokeInt);
	printf("Optimum Unchoke Interval : %d\n", conf->optunchokeInt);
	printf("FileName : %s\n", conf->fileName);
	printf("FileSize : %d\n", conf->fileSize);
	printf("PieceSize : %d\n", conf->pieceSize);
	printf(" *** *********************************** *** \n");

}


/*
 * Print Peer Config Parameters
 */
void print_peer_config(peer_config_t *peer_config /*,int totalNeighbours*/) {
	int i;
	printf("********Printing Peer Config**********\n");
	printf("Total Neighbours : %d\n", totalNeighbours);
	printf("No. of Peer ids less than My PeerID : %d\n", less_count);
	
	for(i=0; i<totalNeighbours; i++) {
		printf("peer Id[%d] : %d", i, peerConfig[i].peerId);
		printf("  peer Address[%d] : %s", i, peerConfig[i].peerAddr);
		printf("  peer Port [%d] : %d", i, peerConfig[i].peerPort);
		printf("  peer Has file ?? : %s\n", peerConfig[i].peerHasFile ? "yes" : "no");
	}
		printf ("This Peer\n");
		printf("peer Id : %d" , thisPeerConfig.peerId);
		printf("  peer Address : %s", thisPeerConfig.thisPeerAddr);
		printf("  peer Port  : %d", thisPeerConfig.thisPeerPort);
		printf("  peer Has file ?? : %s\n", thisPeerConfig.thisPeerHasFile ? "yes" : "no");
	
	printf("****************************************************************\n");
}

/*
 * Print Handshake Message
 */
void print_handshake(handshake_msg_t *hs_msg) {
	printf("---Printing handshake header message---\n");
	printf("Handshake header : %.18s\n", hs_msg->hs_hdr);
	printf("Zero bits : %.10s\n", hs_msg->zero_bits);
	printf("Peer id : %d\n", hs_msg->peer_id);
	printf("----Done printing handshake message---\n");
}

/*
 * Print Global Bitfield
 */
void print_bitfield(bitfield_t bf) {
	int i;
	//printf("*****Printing bit field***\n");
		printf("piece bytes:%d\n", piece_bytes);

	for (i=0; i<piece_bytes; i++) {
		printf("%x", *(bf.bitfield_p + i));
	}
	//		printf("\n***** End Printing bit field***\n");

}
