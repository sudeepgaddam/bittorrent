#include "header.h"

void send_have(int sockd, int piece_id){
	//printf("In send_have piece-id:%d sizeof have_msg: %d\n", piece_id, sizeof(piece_id_msg_t));
	piece_id_msg_t have_msg;
	have_msg.msg_header.msg_length = htonl(5);
	have_msg.msg_header.msg_type = have;
	have_msg.piece_id = htonl(piece_id);
	write(sockd, (void *)&have_msg, sizeof(piece_id_msg_t));
}
void send_have_to_all(int piece_id){
	int i;
	for (i=0; i< totalNeighbours; i++) {
		 send_have(sockfd[i], piece_id);
	}
	//printf("Sent Have to all peer\n");
}
void send_int(int sockd){
	
	printf("In send_int\n");
	msg_header_t msg;
	msg.msg_length = 1;
	msg.msg_length = htonl(msg.msg_length);
	msg.msg_type = interested;
	write(sockd, (void *)&msg, sizeof(msg_header_t));
	
}
void send_not_int(int sockd){
	printf("sending not interested\n");
	msg_header_t msg;
	msg.msg_length = 1;
	msg.msg_length = htonl(msg.msg_length);
	msg.msg_type = not_interested;
	write(sockd, (void *)&msg, sizeof(msg_header_t));
}
void send_choke(int sockd){
	printf("Sending choke to %d\n", peerConfig[sock_index[sockd]].peerId);

		msg_header_t msg;
	msg.msg_length = 1;
	msg.msg_length = htonl(msg.msg_length);
	msg.msg_type = choke;
	write(sockd, (void *)&msg, sizeof(msg_header_t));

}
void send_unchoke(int sockd){
	printf("In send_Unchoke to %d\n", peerConfig[sock_index[sockd]].peerId);
	msg_header_t msg;
	msg.msg_length = 1;
	msg.msg_length = htonl(msg.msg_length);
	msg.msg_type = unchoke;
	write(sockd, (void *)&msg, sizeof(msg_header_t));

}
void send_piece(int piece_id, uint8_t *piece_ptr, int piece_size, int sockd){
	
	//printf("In Send piece\n");
	//printf("piece_id: %d, piece_size: %d sockd: %d\n", piece_id, piece_size, sockd);

	int msg_length = piece_size + 5;
	
	uint8_t *bf_msg = calloc(msg_length+4, sizeof(uint8_t));
	
	
	msg_length = htonl(msg_length);
	int piece_num = htonl(piece_id);
	uint8_t msg_type = piece;
	
	
	
	memcpy(bf_msg, &msg_length, sizeof(int));
	memcpy(bf_msg+4, &msg_type, sizeof(uint8_t));
	memcpy(bf_msg+5, &piece_num, sizeof(int));
	memcpy(bf_msg+9, piece_ptr, piece_size); 
	
	
	int i= 0;
	/*
	for (i=0; i<10; i++) {
		printf("%x",*(bf_msg+i));
	}
	printf("\n");
	* */
	write(sockd, (void *)bf_msg, piece_size+9);
	
	free(bf_msg);
}
void send_request(int piece_id, int sockd){
	piece_id_msg_t req_msg;
	req_msg.msg_header.msg_length = htonl(5);
	req_msg.msg_header.msg_type = request;
	req_msg.piece_id = htonl(piece_id);
	printf("In send_request(), piece-id:%d\n", piece_id);
	write(sockd, (void *)&req_msg, sizeof(piece_id_msg_t));
	
	//Update request bits
	update_request_bits( piece_id,requested_bits); 
}
void send_bitfield(int sockd){
	
	//printf("Sending bitfield to neighbour Piece_bytes:%d\n", piece_bytes);
	uint msg_length = piece_bytes + 1;
	msg_length = htonl(msg_length);
	uint8_t msg_type = bit_field;
	uint8_t *bf_msg = calloc(piece_bytes+5, sizeof(uint8_t));
	memcpy(bf_msg, &msg_length, sizeof(int));
	memcpy(bf_msg+4, &msg_type, sizeof(uint8_t));
	memcpy(bf_msg+5, piece_bits.bitfield_p, piece_bytes);
	int i= 0;
	/*
	for (i=0; i<piece_bytes+5; i++) {
		printf("%x",*(bf_msg+i));
	}
	* */
	printf("\n");
	write(sockd,bf_msg,piece_bytes+5);
}
