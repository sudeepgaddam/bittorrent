#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header.h"
// A utility function to swap to integers
static void swap (int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
// A function to generate a random permutation of arr[]
void randomize ( int *arr, int n )
{
 srand ( thisPeerConfig.peerId );
 int i;
    for ( i = n-1; i > 0; i--)
    {
        int j = rand() % (i+1);
        swap(arr + i, arr + j);
    }
}

void print_randomise ( int *arr, int n )
{
 //srand ( time(NULL) );
 int i;
    for ( i = 0; i <n; i++)
    {
		printf("%d ", *(arr+i));
    }
    printf("\n");
}
